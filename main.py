from UnleashClient import UnleashClient

my_client = UnleashClient(
    url="https://gitlab.com/api/v4/feature_flags/unleash/33566965",
    app_name="my_app",
    instance_id="8ByqJ9TxQVsmbTB_KD3v",
    disable_metrics=True,
    disable_registration=True)

if __name__ == "__main__":

    # init unlease client
    my_client.initialize_client()

    # check if "my-python-app" feature flag is enabled
    if my_client.is_enabled("my-python-app"):
        print("my-python-app is enabled")
    else:
        print("my-python-app is disabled")

    # check if feature flag is enabled for user id abc123
    context = {'userId':'abc123'}
    print("Checking if flag enabled using userID: acb123 ")
    if my_client.is_enabled("specific-user-flag", context):
        print(f"flag enabled for user ID {context['userId']}")

    # the flag is only enabled for specific user id, so this will show disabled
    context = {'userId':'abc456'}
    print("Checking if flag enabled using userID: acb456 ")
    if my_client.is_enabled("specific-user-flag", context):
        print(f"flag enabled for user ID {context['userId']}")
    else:
        print(f"flag disabled for user ID {context['userId']}")
