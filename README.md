# GitLab Feature Flags
> Gitlab feature flags allows enabling/disabling features or portions of code depending on the Feature Flag state in GitLab.

## Feature Flags
---
> Feature flags enable the ability to toggle a feature on or off from GitLab interface
- Feature flags can be accessed via Deploy>Feature Flags
![image-1.png](./readme_images/image-1.png)

- Certain Flags can be enabled/disabled
![image-2.png](./readme_images/image-2.png)

- Flags can also be configured to be enabled for specific users only
![image-3.png](./readme_images/image-3.png)

- There are other strategies other than user id
![image-4.png](./readme_images/image-4.png)

- The included .gitlab-ci.yml is set to output the status's of the flags
![image.png](./readme_images/image.png)

## The Code
---
> Within your script/application you can query for the state of those flags, and determine which feature is included/not included during runtime.
- Example script, printing the status of the feature flags
```python
from UnleashClient import UnleashClient

my_client = UnleashClient(
    url="https://gitlab.com/api/v4/feature_flags/unleash/33566965",
    app_name="my_app",
    instance_id="BmWEJxkCNVZfkNNpwQoc",
    disable_metrics=True,
    disable_registration=True)

if __name__ == "__main__":

    # init unlease client
    my_client.initialize_client()

    # check if "my-python-app" feature flag is enabled
    if my_client.is_enabled("my-python-app"):
        print("my-python-app is enabled")
    else:
        print("my-python-app is disabled")

    # check if feature flag is enabled for user id abc123
    context = {'userId':'abc123'}
    print("Checking if flag enabled using userID: acb123 ")
    if my_client.is_enabled("specific-user-flag", context):
        print(f"flag enabled for user ID {context['userId']}")

    # the flag is only enabled for specific user id, so this will show disabled
    context = {'userId':'abc456'}
    print("Checking if flag enabled using userID: acb456 ")
    if my_client.is_enabled("specific-user-flag", context):
        print(f"flag enabled for user ID {context['userId']}")
    else:
        print(f"flag disabled for user ID {context['userId']}")
```
